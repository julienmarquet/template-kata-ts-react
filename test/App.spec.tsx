import { App } from '@/App';
import {render, screen} from '@testing-library/react';

describe('App', () => {
  it('Should load App', async () => {
    render(<App />)
    expect(screen.queryByText('app')).not.toBeNull()
  });
});
