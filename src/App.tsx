import { FC, useRef, useState } from "react";

export const App: FC = () => {

  const [entrees, setEntrees] = useState<number[]>([]);
  const depenseRef = useRef<HTMLInputElement>(null);
  const revenuRef = useRef<HTMLInputElement>(null);
  return (
    <div className="app">
      <div className="actions">
        <div>
          <input type="number" ref={depenseRef} />
          <button onClick={() => {
            if (depenseRef.current?.value) setEntrees(old => [...old, -Number(depenseRef.current?.value)])
          }}>Ajout dépense</button>
        </div>
        <div>
          <input type="number" ref={revenuRef} />
          <button onClick={() => {
            if (revenuRef.current?.value) setEntrees(old => [...old, Number(revenuRef.current?.value)])
          }}>Ajout revenu</button>
        </div>
      </div>
      <div className="sum">
        <p>Total: {entrees.reduce((acc, item)=>item + acc, 0)} $$$$$$$$$$€€€€€€€€€££££££££</p>
      </div>
      <div className="tresorie">
        {entrees.map((entree, index) => (<div style={{display: 'flex', gap: '1rem'}}>
          <button onClick={() => {
            setEntrees(old => old.filter((_,i) => index !== i ))
          }}>Supprimer</button>
          <p>{entree}</p>
        </div>))}
      </div>
    </div>
  );
};
