# Install


```
    npm install
```

# Run (PORT 8100)

```
    npm start
```

# Test
```
    npm run test
    npm run test:watch
```